.PHONY: image composer-update test

IMAGE_NAME ?= codeclimate/codeclimate-phpmd

SLIM_IMAGE_NAME ?= codeclimate/codeclimate-phpmd:slim

image:
	docker build --tag $(IMAGE_NAME) .

test-image: image
	docker build \
		--build-arg BASE_IMAGE=${IMAGE_NAME} \
		--tag $(IMAGE_NAME)-test \
		--file Dockerfile.test .

composer-update:
	docker run \
	  --rm \
	  --volume $(PWD)/composer.json:/usr/src/app/composer.json:ro \
	  --volume $(PWD)/composer.lock:/usr/src/app/composer.lock \
	  $(IMAGE_NAME) \
	  sh -c 'cd /usr/src/app && composer update'


slim: image
	docker-slim build --tag $(SLIM_IMAGE_NAME) --http-probe=false --exec '/usr/src/app/bin/codeclimate-phpmd' --mount "$$PWD:/code" --workdir '/code' --preserve-path-file 'paths.txt' $(IMAGE_NAME) && prettier --write slim.report.json

test: slim
	container-structure-test test --image $(IMAGE_NAME) --config tests/container-test-config.yaml && container-structure-test test --image $(SLIM_IMAGE_NAME) --config tests/container-test-config.yaml


# test:
# 	@$(MAKE) test-image > /dev/null
# 	docker run \
# 		--rm \
# 		--volume $(PWD)/tests:/usr/src/app/tests \
# 		$(IMAGE_NAME)-test \
# 		sh -c "vendor/bin/phpunit --bootstrap engine.php ./tests"
