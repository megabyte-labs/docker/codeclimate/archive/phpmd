FROM alpine:3

WORKDIR /usr/src/app

RUN adduser -u 9000 -D app

# Install PHP
RUN apk add --no-cache \
      php7 \
      php7-common \
      php7-ctype \
      php7-dom \
      php7-iconv \
      php7-json \
      php7-mbstring \
      php7-opcache \
      php7-openssl \
      php7-pcntl \
      php7-phar \
      php7-simplexml \
      php7-sockets \
      php7-tokenizer \
      php7-xmlwriter \
      php7-xml && \
      ln -sf /usr/bin/php7 /usr/bin/php

# Bring composer
COPY --from=composer /usr/bin/composer /usr/local/bin/composer


# Install Dependencies
COPY composer.* ./
RUN composer install --no-dev && \
    chown -R app:app . && \
    rm -r ~/.composer

# Build Content
COPY bin/build-content ./bin/build-content
RUN apk add --no-cache ruby ruby-json ruby-bigdecimal ruby-dev build-base libxml2-dev libxslt-dev libffi-dev && \
    gem install rdoc nokogiri httparty --no-document && \
    ./bin/build-content && \
    chown -R app:app content && \
    gem uninstall --all rdoc httparty nokogiri && \
    rm -rf $( gem environment gemdir ) && \
    apk del --purge ruby ruby-json ruby-bigdecimal ruby-dev build-base libxml2-dev libxslt-dev libffi-dev && \
    rm -rf /var/cache/* ~/.gem

COPY . ./

RUN find -not \( -user app -and -group app \) -exec chown -R app:app {} \;

USER app

WORKDIR /code
VOLUME /code

CMD ["/usr/src/app/bin/codeclimate-phpmd"]


ARG BUILD_DATE
ARG REVISION
ARG VERSION

LABEL maintainer="Megabyte Labs <help@megabyte.space>"
LABEL org.opencontainers.image.authors="Brian Zalewski <brian@megabyte.space>"
LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.description="Code Climate engine for phpmd"
LABEL org.opencontainers.image.documentation="https://gitlab.com/megabyte-labs/docker/codeclimate/phpmd/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.revision=$REVISION
LABEL org.opencontainers.image.source="https://gitlab.com/megabyte-labs/docker/codeclimate/phpmd.git"
LABEL org.opencontainers.image.url="https://megabyte.space"
LABEL org.opencontainers.image.vendor="Megabyte Labs"
LABEL org.opencontainers.image.version=$VERSION
LABEL space.megabyte.type="code-climate"